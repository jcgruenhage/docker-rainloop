FROM docker.io/matrixdotorg/base-php

ADD rainloop /rainloop
ENV APPDIR=/rainloop
RUN apk add --no-cache \
      php7-curl \
      php7-iconv \
      php7-xml \
      php7-dom \
      php7-openssl \
      php7-json \
      php7-zlib \
      php7-pdo_mysql \
      php7-pdo_pgsql \
      php7-pdo_sqlite \
      php7-sqlite3 \
 && echo "internal /data" >> /etc/Caddyfile 
 
VOLUME /rainloop/data
